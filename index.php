<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Name = ".$sheep->name."<br>"; // "shaun"
echo "Legs = ".$sheep->legs."<br>"; // 4
echo "cold blooded = ".$sheep->cold_blooded."<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())



// index.php

echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Name = ".$kodok->name."<br>"; // "buduk"
echo "Legs = ".$kodok->legs."<br>"; // 4
echo "cold blooded = ".$kodok->cold_blooded."<br>"; // "no"
$kodok->jump() ; // "hop hop"

echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name = ".$sungokong->name."<br>"; // "kera sakti"
echo "Legs = ".$sungokong->legs."<br>"; // 2
echo "cold blooded = ".$sungokong->cold_blooded."<br>"; // "no"
$sungokong->yell(); // "Auooo"


?>